import pandas as pd
import numpy as np
import random
#import quandl
pd.set_option('display.width', 600)
pd.set_option('display.max_columns', 30)
random.seed(42)

PATH= "D:/HopperQuantExercise/hopper/Data/boscun-longitudinal.csv"
OIL_DATA_PATH= "D:/HopperQuantExercise/hopper/Data/CHRIS-CME_UB1.csv"
DATE_COL_LIST=['received_date', 'departure_odate', 'return_odate']
TS_COL_LIST=['received_ms']
#DROP_LIST=["Date", "lowest_price_date", "received_date", "search_id", "trip_ask_id", "trip_ask_id_max"]
#for neural net
DROP_LIST=["Date", "lowest_price_date", "received_date", "search_id", "trip_ask_id_max"]
FEATURES_LAG_LIST= ['total_usd_min', 'Settle', 'available_seats_sum']

def load_data(path):
    return pd.read_csv(path)

def extract_hour(df, ts_col_name, hour_col_name, drop=True):
    df[hour_col_name]= pd.to_datetime(df[ts_col_name], unit='ms').dt.hour
    if drop:
        df.drop([ts_col_name], axis=1, inplace=True)
    return df

def extract_from_date(df, dat_col, new_col, month=True, day=True, day_of_week=True,drop=True):
    if month:
        df['{}_month'.format(new_col)]= df[dat_col].str[5:7]
    if day:
        df['{}_day'.format(new_col)] = df[dat_col].str[8:10].astype(int)
    if day_of_week:
        df['{}_day_of_week'.format(new_col)] = pd.to_datetime(df[dat_col], format="%Y-%m-%d").dt.dayofweek
    if drop:
        df.drop([dat_col], axis=1, inplace=True)
    return df

def clean_dates(df, date_col_list, ts_col_list, month=True, day=True, day_of_week=True, drop=True,exclude_from_drop= "received_date"):
    for this_date in date_col_list:
        if this_date in exclude_from_drop:
            df=extract_from_date(df=df, dat_col= this_date, new_col= this_date, month=month, day=day, day_of_week=day_of_week, drop=False)
        else:
            df = extract_from_date(df=df, dat_col=this_date, new_col=this_date, month=month, day=day,day_of_week=day_of_week, drop=drop)
    for this_ts in ts_col_list:
        new_name= this_ts.split("_")[0]+ "_hour"
        df=extract_hour(df=df, ts_col_name= this_ts, hour_col_name=new_name, drop=True)
    return df

def create_new_cols(df):
    """
    an asked trip is a combination of a departure date and a return date
    has_stop: whether there is a stop in either of the trip
    """
    df['trip_ask_id']= df['departure_odate']+"--" +df['return_odate']
    df['has_stop']= np.where((df['outgoing_stops']>0) | (df['returning_stops']>0), 1,0)
    df['multi_carrier']= np.where(df['validating_carrier']!=df['major_carrier_id'],1,0)
    return df



def agregate_by_search(df, grp_by_list):
    df_group= df.groupby(grp_by_list,as_index=False ).agg({'trip_index': ['count']
                                                           , 'total_usd': ['min','mean', 'max', 'std']
                                                           , 'validating_carrier':('nunique')#TO BE CONTINUED
                                                           , 'outgoing_duration':['min', 'mean', 'max']
                                                           , 'outgoing_stops':['min', 'mean', 'max']
                                                           , 'returning_duration':['min', 'mean', 'max']
                                                           , 'returning_stops':['min', 'mean', 'max']
                                                           , 'total_stops':['min', 'mean', 'max','std']
                                                           , 'advance':['max']
                                                           , 'length_of_stay':['max']
                                                           , 'includes_saturday_night_stay':['max']
                                                           , 'available_seats':['sum','min', 'mean', 'max', 'std']
                                                           , 'lowest_cabin_class': ['max']
                                                           #, 'highest_cabin_class': ['min'] # to be verfied
                                                           , 'trip_ask_id':['max']
                                                           , 'multi_carrier':['max']
                                                           , 'received_date_month':['max']
                                                           , 'received_date_day':['max']
                                                           , 'received_date_day_of_week': ['max']
                                                           , 'departure_odate_month':['max']
                                                           , 'departure_odate_day':['max']
                                                           , 'departure_odate_day_of_week':['max']
                                                           , 'return_odate_month':['max']
                                                           , 'return_odate_day':['max']
                                                           , 'return_odate_day_of_week':['max']
                                                           , 'received_hour':['max']})

    df_group.columns = ["_".join(x) for x in df_group.columns.ravel() if x not in grp_by_list]
    df_group.rename(index=str, columns={'search_id_': 'search_id', 'has_stop_': 'has_stop'}, inplace=True)
    #get the value of some variables when the price was lowest
    df_lowestPrice_search =df.groupby(grp_by_list, as_index=False).total_usd.min()
    df_full= pd.merge(df, df_lowestPrice_search, on = grp_by_list)
    df_full= df_full.loc[df_full['total_usd_x']==df_full['total_usd_y'], grp_by_list+ ['validating_carrier', 'outgoing_duration', 'returning_duration','total_stops', 'major_carrier_id','available_seats', 'multi_carrier','trip_ask_id', 'received_date']]
    df_full.drop_duplicates(grp_by_list, inplace =True)# drop duplicate in case several offers for same lowest price. Maybe computing number of duplicates
    df_group= pd.merge(df_group, df_full, on = grp_by_list)
    return df_group

def get_oil_prices(oil_data_path):
    oil= pd.read_csv(oil_data_path)
    oil= oil.loc[(oil["Date"]>="2015-07-19") & (oil["Date"]<="2016-12-31"), ["Date", "Settle"]]
    return oil

def add_oil_data(df, df_oil):
    df_merged=pd.merge(df, df_oil, left_on="received_date", right_on="Date", how="left")
    return df_merged

def create_time_to_best_price(df, grp_by_list):
    min_price_by_trip= df.groupby(grp_by_list).total_usd_min.min()
    df_merged= pd.merge(df, min_price_by_trip, on=grp_by_list)
    df_min_price_date= df_merged.loc[(df_merged["total_usd_min_x"]==df_merged["total_usd_min_y"]), grp_by_list+["received_date", "total_usd_min_y"]] # garder total_usd_min_y as absolute minimum to have net predict both time to lowest price and absolust lowest price
    df_min_price_date=df_min_price_date.groupby(grp_by_list, as_index=False).agg({'received_date': ['min'], "total_usd_min_y":["min"]})
    df_min_price_date.columns = ["_".join(x) for x in df_min_price_date.columns.ravel() if x not in grp_by_list]
    df_min_price_date.rename(index=str, columns={"received_date_min":"lowest_price_date"
                                                 , "total_usd_min_y_min":"lowest_price"
                                                 , "trip_ask_id_":"trip_ask_id"
                                                 , "has_stop_": "has_stop"
                                                 }, inplace=True)
    df_grp= pd.merge(df, df_min_price_date, on=grp_by_list)
    df_grp["time_to_best_price"]= (pd.to_datetime(df_grp['lowest_price_date'], format="%Y-%m-%d")-pd.to_datetime(df_grp['received_date'], format="%Y-%m-%d")).dt.days
    return df_grp

def additional_time_to_best_price(df, grp_by_list):
    """
    When best date is passed, find next best date and lowest price
    """
    while df.loc[df["time_to_best_price"]<0, :].shape[0] >0:
        #print("still {} negative values".format(df.loc[df["time_to_best_price"]<0, :].shape[0]))
        df_ok= df.loc[df["time_to_best_price"]>=0, :]
        df_bad= df.loc[df["time_to_best_price"]<0, :]
        assert(df.shape[0]== df_ok.shape[0]+ df_bad.shape[0], "debug0: inconsistant number of rows")
        df_bad.drop(["lowest_price_date", "lowest_price"], axis=1, inplace=True)
        df_bad_grp= create_time_to_best_price(df_bad, grp_by_list)
        assert(df_bad.shape[0]==df_bad_grp.shape[0], "debug1: inconsistant number of rows 1")
        df = pd.concat([df_ok,df_bad_grp] ,ignore_index=True, sort=True)
    return df

def lag_data(df, features_lag, pass_length):
    """
    function to lag the data, ie adding the value of a feature at t-n.
    In order to lag the data we will add to our dataframe (concat axis=1) a dataframe wich is shifted by n rows.
    This way, we will have side by side the price at time t and time t-n
    """
    df["trip_ask_id"]= df["trip_ask_id"] +"_"+ df["has_stop"].astype(str)
    df.reset_index(drop=True, inplace=True)
    df.sort_values(by=["trip_ask_id", "received_date"], ascending=True, inplace=True)
    features_lag.insert(0, "trip_ask_id")
    df_features= df[features_lag]
    cols, names= [], []
    for i in range(pass_length, 0, -1):
        cols.append(df_features.shift(i ))
        names += [('{}_t-{}'.format(j, i)) for j in features_lag]
    t_seri = pd.concat(cols, axis=1)
    t_seri.columns = names
    #df_final = pd.concat([df, t_seri],axis=1, ignore_index=True)
    df_final = pd.concat([df.loc[:, (h for h in df.columns if (h not in features_lag) or h == "trip_ask_id")], t_seri],axis=1)
    for i in range(pass_length, 0, -1):
        #df_final.loc[(df_final['trip_ask_id'] != df_final['{}_t-{}'.format('trip_ask_id', i)]), [col for col in df_final.columns if '_t-{}'.format(i) in col]] = np.nan
        df_final.loc[df_final['trip_ask_id'] != df_final['{}_t-{}'.format('trip_ask_id', i)], [col for col in df_final.columns if '_t-{}'.format(i) in col]] = np.nan
    #df_final = df_final.drop((col for col in df_final.columns if 'trip_ask_id_t' in col),axis=1)
    features_lag = features_lag[1:]
    df_features = df_features[features_lag]
    df_final = pd.concat([df_final, df_features], axis=1)
    return df_final

def smoothing( df, features_lag, a=.7, drop=True):
    for col in features_lag:
        df['{}_smooth'.format(col)]= a*(((df['{}'.format(col)]-df['{}_t-1'.format(col)])/df['{}_t-1'.format(col)])\
                                       +(1-a)*((df['{}_t-1'.format(col)]-df['{}_t-2'.format(col)])/df['{}_t-2'.format(col)])\
                                       +((1-a)**2)*((df['{}_t-2'.format(col)]-df['{}_t-3'.format(col)])/df['{}_t-3'.format(col)])\
                                       +((1-a)**3)*((df['{}_t-3'.format(col)]-df['{}_t-4'.format(col)])/df['{}_t-4'.format(col)])\
                                       +((1-a)**4)*((df['{}_t-5'.format(col)]-df['{}_t-4'.format(col)])/df['{}_t-5'.format(col)]))#TBD find better smoothing method that take into account different time between observations
    if drop:
        df.drop([col for col in df.columns if 't-' in col], axis=1, inplace=True)
    return df



def split_data(df, nb_test_months):
    """
    in order not to bias our test set we will substract all the trips for n months, those trips will be our test set
    """
    departure_months=df["trip_ask_id"].str[:7].unique().tolist()
    test_months= random.sample(departure_months,nb_test_months)
    train= df.loc[(~df["trip_ask_id"].str[:7].isin(test_months)), :]
    test= df.loc[(df["trip_ask_id"].str[:7].isin(test_months)), :]
    return train, test

def remove_unnecessary_col(df, col_to_drop_list):
    df.drop(col_to_drop_list, axis=1, inplace=True)
    return df


def load_and_preprocess(split_nb=6):
    df= load_data(PATH)
    print("dataset loaded")
    df= create_new_cols(df)
    df= clean_dates(df, DATE_COL_LIST, TS_COL_LIST)
    print("starting aggregations")
    df_grp= agregate_by_search(df, ['search_id', 'has_stop'])
    print("adding oil prices")
    oil=get_oil_prices(OIL_DATA_PATH)
    df_grp= add_oil_data(df_grp, oil)
    print("geting best date by trip")
    df_grp = create_time_to_best_price(df_grp, ['trip_ask_id', 'has_stop'])
    df_grp = additional_time_to_best_price(df_grp, ['trip_ask_id', 'has_stop'])
    print("splitting dataset")
    df_grp= lag_data(df_grp, FEATURES_LAG_LIST.copy(), 5)
    df_grp= smoothing(df_grp,  FEATURES_LAG_LIST)
    train, test=split_data(df_grp, split_nb)
    train= remove_unnecessary_col(train, DROP_LIST)
    test = remove_unnecessary_col(test, DROP_LIST)
    return train, test



if __name__=='__main__':
    load_and_preprocess()
