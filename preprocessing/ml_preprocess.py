import pandas as pd
import numpy as np
import sklearn
from sklearn.preprocessing import StandardScaler
import h2o
from h2o.estimators import H2ORandomForestEstimator

from h2o.grid.grid_search import H2OGridSearch
import matplotlib.pyplot as plt
import hopper.preprocessing.preprocessing as pr

def create_train_test(save=True):
    train, test= pr.load_and_preprocess()
    if save:
        train.to_csv("D:/HopperQuantExercise/hopper/Data/train_lag.csv", index=False)
        test.to_csv("D:/HopperQuantExercise/hopper/Data/test_lag.csv", index=False)
    return train, test

def load_train_test():
    train= pd.read_csv("D:/HopperQuantExercise/hopper/Data/train_lag.csv")
    test= pd.read_csv("D:/HopperQuantExercise/hopper/Data/test_lag.csv")
    return train, test

HYP_PARAM={'ntrees':[50,100,150,200]
         ,'min_rows':[1,2,5,10,25,50]
         ,'sample_rate':[.8,1.]
         ,'col_sample_rate_per_tree': [0.2, 0.5, 1.0]
         ,'max_depth': [3, 5, 9]}

SEARCH= {'strategy': 'RandomDiscrete',
         'max_runtime_secs': 450,
         'seed': 42}
N_FOLDS=3

PRED_COLS= ["time_to_best_price", "lowest_price"]

class ml_preprocess():
    def __init__(self, train, test):
        self.train= train
        self.test = test

    def init_num_var(self, df, exclude_type=['object', 'bool'], exclude=PRED_COLS):
        self.num_feat=[]
        for col in df.columns:
            if df[col].dtypes not in exclude_type and col not in exclude:
                self.num_feat.append(col)

    def scale_features(self, df, num_feat, scaler=None):
        if scaler==None:
            self.scaler= StandardScaler()
            self.scaler.fit(df[num_feat])
        else:
            self.scaler= scaler
        df[num_feat]=self.scaler.transform(df[num_feat])
        return df
    def get_scaler(self):
        return self.scaler

    def  init_h20_clust(self):
        h2o.init()

    def init_h2o_frame(self, df):
        return h2o.H2OFrame(df)

    def train_rf(self, train, y_col, more_remove=None):
        x= train.columns
        x.remove(y_col)
        if more_remove!=None:
            x.remove(more_remove)
        self.rf_grid = H2OGridSearch(model=H2ORandomForestEstimator(nfolds=N_FOLDS),
                                  grid_id='var_select_grid',
                                  hyper_params=HYP_PARAM,
                                  search_criteria=SEARCH)
                                  #, nfolds = N_FOLDS)
        self.rf_grid.train(x=x
                      , y=y_col
                      , training_frame=train
                      , seed=42)
        grid_sorted=self.rf_grid.get_grid(sort_by='rmse', decreasing=True)
        best_rf = grid_sorted.models[0]
        var_imp=best_rf.varimp()
        var_imp= pd.DataFrame(var_imp)
        var_imp.columns=['variable','relative_importance','scaled_importance','percentage']
        return var_imp

    def viz_varimp(self, var_imp_frame):
        print(var_imp_frame)
        plt.plot(var_imp_frame['relative_importance'])

    def variable_selection(self, df,n_keep, var_imp_frame):
        x_new= var_imp_frame['variable'][:n_keep+1].to_list()
        #TBD: put unused variables in a PCA
        return df[x_new + PRED_COLS]

    def shut_down_h2o(self):
        h2o.cluster().shutdown()

    def scale(self):
        self.init_num_var(self.train, exclude_type=['object', 'bool'])
        self.train_sc= self.scale_features( df=self.train, num_feat=self.num_feat, scaler=None)
        scaler= self.get_scaler()
        self.test_sc = self.scale_features( df=self.test, num_feat= self.num_feat, scaler=scaler)




"""
process= ml_preprocess(train, test)
process.scale()
process.init_h20_clust()
train_h2o= process.init_h2o_frame(process.train_sc)
var_imp= process.train_rf(train_h2o, y_col="lowest_price", more_remove="time_to_best_price")
process.viz_varimp(var_imp)
train_reduce= process.variable_selection( df=process.train_sc,n_keep=15, var_imp_frame=var_imp)
test_reduce= process.variable_selection( df=process.train_sc,n_keep=15, var_imp_frame=var_imp)


#h2o.init()
train_h2o=h2o.H2OFrame(train_reduce)
test_h2o= h2o.H2OFrame(test_reduce)

x= train_reduce.columns.tolist()
y_time_to_best_price="lowest_price"
y_lowest_price="time_to_best_price"
x.remove(y_time_to_best_price)
x.remove(y_lowest_price)

aml = H2OAutoML(max_models=20, seed=43)
aml.train(x=x, y=y_time_to_best_price, training_frame=train_h2o)

lb= aml.leaderboard
print(lb)
h2o.save_model(aml.leader, "D:/HopperQuantExercise/hopper/models/lowest_price_model_reduce15")

pred= aml.leader.predict(test_h2o)
test2= pd.concat([test_reduce, pred.as_data_frame()], axis=1)

print(aml.leader.model_performance(test_h2o))

test.head()
plt.plot(test2.time_to_best_price[:500],  label='truth')
plt.plot(test2.predict[:500], label='pedict')

plt.scatter(test2.time_to_best_price[1500:2000], abs(test2.time_to_best_price[1500:2000]-test2.predict[1500:2000])
            ,label = np.where((test2.time_to_best_price[1500:2000]-test2.predict[1500:2000])>=0, 1,0))

colors = ("red",  "blue")
#process.shut_down_h2o()
test2['signe']=np.where((test2.time_to_best_price-test2.predict)>=0, 1,0)
test2['error']= abs(test2.time_to_best_price-test2.predict)

target= np.where((test2.time_to_best_price[1500:2000]-test2.predict[1500:2000])>=0, 1,0)
f, ax = plt.subplots(1)
for i in np.unique(test2['signe']):
    plt.scatter(test2.loc[test2['signe']==i,['time_to_best_price'] ][:500], test2.loc[test2['signe']==i,['error'] ][:500], label=i)
ax.legend()

#essayer avec 20
process.shut_down_h2o()

np.mean(abs(test2.lowest_price- test2.predict))=17.02
"""