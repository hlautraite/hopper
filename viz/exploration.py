import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
pd.set_option('display.width', 600)
pd.set_option('display.max_columns', 30)
PATH= "D:/HopperQuantExercise/hopper/Data/boscun-longitudinal.csv"

def load_data(path):
    return pd.read_csv(path)

def boxplot(dep_var, var_list):
    for this_var in var_list:
        df.boxplot(dep_var, by=this_var)

def extract_hour(df, ts_col_name, hour_col_name, drop=True):
    df[hour_col_name]= pd.to_datetime(df[ts_col_name], unit='ms').dt.hour
    if drop==True:
        df.drop([ts_col_name], axis=1, inplace=True)
    return df

def extract_from_date(df, dat_col, new_col, month=True, day=True, drop=True):
    if month==True:
        df['{}_month'.format(new_col)]= df[dat_col].str[5:7]
    if day==True:
        df['{}_day'.format(new_col)] = df[dat_col].str[9:10].astype(int)
    if drop==True:
        df.drop([dat_col], axis=1, inplace=True)

def clean_dates(df, date_col_list, ts_col_list, month=True, day=True, drop=True):
    for this_date in date_col_list:
        extract_from_date(df=df, dat_col= this_date, new_col= this_date, month=month, day=day, drop=drop)
    for this_ts in ts_col_list:
        new_name= this_ts.split("_")[0]+ "_hour"
        extract_hour(df=df, ts_col_name= this_ts, hour_col_name=new_name, drop=True)

#CREER COL POUR LA DEMANDE

BY_VAR=['length_of_stay', 'lowest_cabin_class', 'total_stops', 'available_seats', 'includes_saturday_night_stay']

df= load_data(PATH)
df.shape
df.dtypes
df.head()
df.describe()
df.total_usd.hist(bins=100, label= "lowest price by trip ")

boxplot('total_usd', BY_VAR)

df_sample= train.sample(frac=.01)
plt.scatter(df_sample.time_to_best_price, df_sample.total_usd_min)
df.corr
df['days_to_departure'] =(pd.to_datetime(df.departure_odate, format='%Y-%m-%d') - pd.to_datetime(df.received_date, format='%Y-%m-%d')).components.days


df_agreg= df.groupby(df['departure_odate']).agg({'total_usd':np.min})
plt.plot(df_agreg)
df_agreg2= df.groupby(df['received_date']).agg({'total_usd':np.min})
plt.plot(df_agreg2)

df_agreg3= df.groupby(['departure_odate', 'return_odate']).count()

#nb research moyenne par trip ask: 21.55

train= pd.read_csv("D:/HopperQuantExercise/hopper/Data/train_0.csv")
train.lowest_price.hist(bins=100)

train.time_to_best_price.hist(bins=100)