import numpy as np
import pandas as pd
import keras
import _tkinter
import sklearn
from sklearn.preprocessing import LabelEncoder, OneHotEncoder, MinMaxScaler, StandardScaler
import hopper.preprocessing.ml_preprocess as prml
from keras.layers import Input, Dense, Dropout
from keras.models import Model
from keras.callbacks import History, ModelCheckpoint, TerminateOnNaN, EarlyStopping
import hopper.preprocessing.preprocessing as pr
import matplotlib.pyplot as plt


#keep is top 20 from varimp time to best priice h2o
KEEP= ['advance_max', 'return_odate_month_max', 'major_carrier_id', 'outgoing_duration_min', 'returning_duration_max', 'total_usd_min', 'available_seats_mean', 'outgoing_duration_max', 'returning_duration', 'outgoing_duration', 'departure_odate_month_max', 'returning_duration_mean', 'multi_carrier_max', 'total_usd_mean', 'available_seats_sum', 'trip_index_count', 'departure_odate_day_max', 'return_odate_day_of_week_max', 'multi_carrier', 'validating_carrier_nunique', 'returning_stops_min']
PRED_VAR=['time_to_best_price', 'lowest_price']
PATH="D:/HopperQuantExercise/hopper/models/multi_objective/"



def select_var(df, var_keep):
    return df[var_keep+ PRED_VAR]

def get_cat_var(df):
    cat_var=[]
    for col in df.columns:
        if df[col].dtypes=='object':
            cat_var.append(col)
    return cat_var

def split_features(df, pred_var):
    df_x= df[[col for col in df.columns if col not in pred_var]]
    df_y1= pd.DataFrame(df['time_to_best_price'])
    df_y2= pd.DataFrame(df['lowest_price'])
    return df_x,df_y1, df_y2

def one_hot_encode(df, cat_var, encoder_list= None, ohe= None):
    if not encoder_list:
        encoder_list=[]
    for i in range(len(cat_var)):
        if not encoder_list:
            le= LabelEncoder()
            le.fit(df[cat_var[i]])
        else:
            le= encoder_list[i]
        df[cat_var[i]]= le.transform(df[cat_var[i]])
        encoder_list.append(le)
    if not ohe:
        ohe= OneHotEncoder()
        ohe.fit(df[cat_var])
    else:
        ohe=ohe
    df_ohe= ohe.transform(df[cat_var]).toarray()
    return pd.concat([df[[col for col in df.columns if col not in cat_var]], pd.DataFrame(df_ohe)], axis=1), encoder_list, ohe

def standardize_x(df, cat_var, scaler=None):
    if not scaler:
        scaler= StandardScaler()
        scaler.fit(df[[col for col in df.columns if col not in cat_var]])
    else:
        scaler=scaler
    df.loc[:, [col for col in df.columns if col not in cat_var]]= scaler.transform(df[[col for col in df.columns if col not in cat_var]])
    return df, scaler

def normalize_y(df_y1, df_y2, norm1=None, norm2=None):
    if (not norm1) & (not norm2):
        norm1= MinMaxScaler()
        norm2= MinMaxScaler()
        norm1.fit(df_y1)
        norm2.fit(df_y2)
    else:
        norm1=norm1
        norm2=norm2
    df_y1= norm1.transform(df_y1)
    df_y2= norm2.transform(df_y2)
    return df_y1, df_y2, norm1, norm2

def nn_preprocess(create=False,save=True):
    if create:
        train, test = pr.load_and_preprocess(8)
        test, valid = pr.split_data(test, 4)
        if save:
            train.to_csv(PATH+"train.csv", index=False)
            test.to_csv(PATH+"test.csv", index=False)
            valid.to_csv(PATH+"valid.csv", index=False)
    else:
        train= pd.read_csv(PATH+"train.csv")
        test = pd.read_csv(PATH + "test.csv")
        valid = pd.read_csv(PATH + "valid.csv")
    train= select_var(train, KEEP)
    test = select_var(test, KEEP)
    valid = select_var(valid, KEEP)
    cat_var= get_cat_var(train)
    train_x, train_y1, train_y2= split_features(train, PRED_VAR)
    test_x, test_y1, test_y2= split_features(test, PRED_VAR)
    valid_x, valid_y1, valid_y2= split_features(valid, PRED_VAR)
    train_x, le, ohe= one_hot_encode(train_x, cat_var)
    test_x, _, __= one_hot_encode(test_x, cat_var, le, ohe)
    valid_x, _, __= one_hot_encode(valid_x, cat_var, le, ohe)
    train_x, scaler= standardize_x(train_x, cat_var)
    test_x, _= standardize_x(test_x, cat_var, scaler)
    valid_x, _= standardize_x(valid_x, cat_var, scaler)
    train_y1, train_y2, norm1, norm2= normalize_y(train_y1, train_y2)
    test_y1, test_y2, _, __ = normalize_y(test_y1, test_y2, norm1, norm2)
    valid_y1, valid_y2, _, __ = normalize_y(valid_y1, valid_y2, norm1, norm2)
    return train_x, train_y1, train_y2, test_x, test_y1, test_y2, valid_x, valid_y1, valid_y2, norm1, norm2

train_x, train_y1, train_y2, test_x, test_y1, test_y2, valid_x, valid_y1, valid_y2, norm1, norm2= nn_preprocess(False, False)

class neural_net():
    def __init__(self, path):
        self.path= path
        self.init_net()
        self.init_callbacks()

    def init_net(self):
        #define layers
        inputs_layer = Input(shape=(30,))
        drop_out1= Dropout(.5)(inputs_layer)
        Dens1 = Dense(20, activation='relu')(drop_out1)
        drop_out2= Dropout(.5)(Dens1)
        Dense2 = Dense(20, activation='relu')(drop_out2)
        drop_out3 = Dropout(.5)(Dense2)
        #Dens3 = Dense(20, activation='relu')(drop_out3)
        #drop_out4 = Dropout(.5)(Dens3)
        output_time_to_next_price= Dense(1, activation='relu')(drop_out3)
        output_time_lowest_price = Dense(1, activation='relu')(drop_out3)

        #define net
        self.net = Model(inputs=inputs_layer, outputs=[output_time_to_next_price, output_time_lowest_price])

        #compile net
        self.net.compile(optimizer=keras.optimizers.Adam(lr=0.0001), loss='mse', loss_weights=[.5, .5], metrics=['mae'])

    def init_callbacks(self):
        self.history = History()
        self.nanterminator = TerminateOnNaN()
        self.filepath = self.path+ "multi_objective_3dense.hdf5"
        # early stop
        self.early_stop = EarlyStopping(monitor='val_loss', min_delta=0.0001, patience=500, verbose=1, mode='min')
        # pannes bin
        self.checkpoint = ModelCheckpoint(self.filepath, monitor='val_loss', verbose=1, save_best_only=True, mode='min')

    def show_model_summary(self):
        print(self.net.summary())

    def train(self, train_x, train_y_list, epochs, batch_size, valid_x, valid_y_list):
        self.net.fit(train_x
                     , train_y_list
                     , epochs=epochs
                     , batch_size=batch_size
                     , validation_data=(valid_x, valid_y_list)
                     , shuffle=True
                     , callbacks=[self.nanterminator, self.history, self.checkpoint, self.early_stop]
                     , verbose= 2)

nn= neural_net(PATH)
nn.show_model_summary()
nn.train(train_x=train_x, train_y_list=[train_y1, train_y2], epochs= 1500, batch_size=64,\
         valid_x= valid_x, valid_y_list=[valid_y1, valid_y2])

def plot_history(nn):
    plt.plot(nn.history.history['loss'],    label='training')
    plt.plot(nn.history.history['val_loss'],label='validation')
    plt.title('loss')
    plt.legend()

nn.net.load_weights(nn.path+ "multi_objective.hdf5")

pred_y1, pred_y2= nn.net.predict(test_x)

print(norm1.inverse_transform([[np.mean(abs(test_y1-pred_y1))]]))
print(norm2.inverse_transform([[np.mean(abs(test_y2-pred_y2))]]))

print(norm1.inverse_transform([[np.sqrt(np.mean((test_y1-pred_y1)**2))]]))
print(norm2.inverse_transform([[np.sqrt(np.mean((test_y2-pred_y2)**2))]]))


plt.plot(test_y1, label='truth')
plt.plot(pred_y1, label='pred')