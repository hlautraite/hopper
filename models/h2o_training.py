import pandas as pd
import numpy as np
import hopper.preprocessing.ml_preprocess as pr
import h2o
from h2o.automl import H2OAutoML
import pickle


class training():
    def __init__(self, pred_var, other_pred, model_name, path, train, test):
        print("creating training object")
        self.pred_var= pred_var
        self.other_pred= other_pred
        self.model_name= model_name
        self.path=path
        self.train= train
        self.test=test

    def load_data(self, create=False):
        if create:
            self.train, self.test = pr.create_train_test()
        else:
            self.train, self.test = pr.load_train_test()

    def init_pre_process(self):
        self.process = pr.ml_preprocess(self.train, self.test)
        self.process.scale()
        self.process.init_h20_clust()

    def train_var_select(self):
        train_h2o = self.process.init_h2o_frame(self.process.train_sc)
        self.var_imp = self.process.train_rf(train_h2o, y_col=self.pred_var, more_remove=self.other_pred)
        self.process.viz_varimp(self.var_imp)

    def save_var_imp(self):
        with open(self.path+'var_imp_{}'.format(self.model_name), 'wb') as var_imp_file:
            pickle.dump(self.var_imp, var_imp_file)

    def load_var_imp(self):
        with open(self.path+'var_imp_{}'.format(self.model_name), 'rb') as var_imp_file:
            self.var_imp= pickle.load(var_imp_file)

    def prepare_frames(self, n_keep):
        self.train_reduce = self.process.variable_selection(df=self.process.train_sc, n_keep=n_keep, var_imp_frame=self.var_imp)
        self.test_reduce = self.process.variable_selection(df=self.process.test_sc, n_keep=n_keep, var_imp_frame=self.var_imp)
        self.train_reduce_h2o = h2o.H2OFrame(self.train_reduce)
        self.test_reduce_h2o = h2o.H2OFrame(self.test_reduce)
        self.x_aml = self.train_reduce.columns.tolist()
        self.x_aml.remove(self.pred_var)
        self.x_aml.remove(self.other_pred)

    def train_AutoML(self, max_models=30):
        self.aml = H2OAutoML(max_models=max_models, seed=43)
        self.aml.train(x=self.x_aml, y=self.pred_var, training_frame=self.train_reduce_h2o)
        self.model= self.aml.leader

    def save_aml(self, n_keep):
        h2o.save_model(self.aml.leader, self.path+ self.model_name+ str(n_keep))

    def load_aml(self, n_keep):
        #!!! NOT WORKING!!!
        #subprocess to get exact model name
        self.model= h2o.load_model(self.path+ self.model_name+ str(n_keep))

    def eval_on_test(self):
        pred= self.model.predict(self.test_reduce_h2o).as_data_frame()
        self.test_scored= pd.concat([self.test_reduce, pred], axis=1)
        print(self.model.model_performance(self.test_reduce_h2o))
        mae= np.mean(abs(self.test_scored[self.pred_var]- self.test_scored['predict']))
        print("Average error for model when predicting {}: {}".format(self.pred_var, mae))

    def var_select(self):
        self.train_var_select()
        self.save_var_imp()

    def train_models(self, n_keep):
        self.prepare_frames(n_keep=n_keep)
        self.train_AutoML()
        self.save_aml(n_keep=n_keep)
        self.eval_on_test()





"""
def prepare_frame(n_keep=20, ml_preprocess=process):
    train_reduce = process.variable_selection(df=process.train_sc, n_keep=n_keep, var_imp_frame=var_imp)
    test_reduce = process.variable_selection(df=process.train_sc, n_keep=n_keep, var_imp_frame=var_imp)
    train_h2o = h2o.H2OFrame(train_reduce)
    test_h2o = h2o.H2OFrame(test_reduce)
    x = train_reduce.columns.tolist()
    y_time_to_best_price = "lowest_price"
    y_lowest_price = "time_to_best_price"
    x.remove(y_time_to_best_price)
    x.remove(y_lowest_price)


train_reduce = process.variable_selection(df=process.train_sc, n_keep=20, var_imp_frame=var_imp)
test_reduce = process.variable_selection(df=process.train_sc, n_keep=20, var_imp_frame=var_imp)
train_h2o = h2o.H2OFrame(train_reduce)
test_h2o = h2o.H2OFrame(test_reduce)
x = train_reduce.columns.tolist()
y_time_to_best_price = "lowest_price"
y_lowest_price = "time_to_best_price"
x.remove(y_time_to_best_price)
x.remove(y_lowest_price)
aml = H2OAutoML(max_models=30, seed=43)
aml.train(x=x, y=y_time_to_best_price, training_frame=train_h2o)
"""