import numpy as np
import pandas as pd
import hopper.models.h2o_training as tr
import hopper.preprocessing.ml_preprocess as pr
import h2o
PATH="D:/HopperQuantExercise/hopper/models/lowest_price/"

train, test= pr.load_train_test()

training= tr.training('lowest_price', 'time_to_best_price', 'lowest_price', PATH, train, test)
training.load_data()
training.init_pre_process()
training.var_select()
#training.load_var_imp()
training.train_models(10)
training.eval_on_test()

